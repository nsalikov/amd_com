# -*- coding: utf-8 -*-
import re
import json
import scrapy
from scrapy.shell import inspect_response
from scrapy.exceptions import CloseSpider, DontCloseSpider


class PartsSpider(scrapy.Spider):
    name = 'parts'
    allowed_domains = ['amd.com']
    start_urls = [
        'https://www.amd.com/en/products/specifications/processors/',
    ]

    api_url = 'https://www.amd.com/en/products-rest?_format=json'

    headers = {
        'content-type': 'application/json;odata=verbose',
        'Accept': 'application/json;odata=verbose',
        'X-Requested-With': 'XMLHttpRequest',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36',
        'Referer': None,
    }

    only_summary = True

    idle = False
    urls = {}


    @classmethod
    def from_crawler(cls, crawler, *args, **kwargs):
        spider = super().from_crawler(crawler, *args, **kwargs)
        crawler.signals.connect(spider.spider_idle, signal=scrapy.signals.spider_idle)
        return spider


    def start_requests(self):
        for url in self.start_urls:
            self.headers['Referer'] = url
            yield scrapy.Request(self.api_url, headers=self.headers, callback=self.parse_api, dont_filter=True)


    def parse_api(self, response):
        try:
            data = json.loads(response.text)
        except Exception as e:
            self.logger.error('Unable to parse json', exc_info=True)
            self.logger.debug('Response:\n{}'.format(response.text))
            return

        for d in data:
            for term_id in d['term_id'].split(','):
                term_id = term_id.strip()
                self.urls[term_id] = response.urljoin(d['url'])


    def parse_html(self, response):
        # inspect_response(response, self)

        headers = [th.xpath('./text()').extract_first() for th in response.xpath('//*[@id="spec-table"]/thead/tr/th')]
        headers = [h.strip() if type(h) is str else '' for h in headers]

        for tr in response.xpath('//*[@id="spec-table"]/tbody/tr'):
            try:
                values = [td.xpath('./text()').extract_first() for td in tr.xpath('./td')]
                d = dict([t for t in zip(headers, values) if t[0]])
            except Exception as e:
                self.logger.error('Unable to parse specs', exc_info=True)
            else:
                cl = tr.xpath('.//td[contains(@class, "term") or contains(@class, "entity")]/@class').extract_first()
                url = get_url(self.urls, cl)
                if url:
                    d['url'] = url

                    if self.only_summary:
                        yield d
                    else:
                        yield scrapy.Request(url, meta={'item': d}, callback=self.parse_item)


    def parse_item(self, response):
        meta_d = response.meta['item']

        specs = response.css('#product-specs')

        if not specs:
            self.logger.warning('Unable to find the specs: {}'.format(response.request.url))
            return

        d = {'url': response.url}

        for fieldset in specs.css('fieldset'):
            fieldset_name = clean_text(fieldset.css('.fieldset-legend ::text').extract_first())

            if fieldset_name not in d:
                d[fieldset_name] = {}

            for field in fieldset.css('.field'):
                field_name = [clean_text(v).strip('*') for v in field.css('.field__label ::text').extract()]
                field_name = ' '.join(filter(None, field_name))

                field_values = [clean_text(v) for v in field.css('.field__item ::text, .field__items ::text').extract()]
                field_values = [v for v in field_values if not v.startswith('*')]
                field_values = list(filter(None, field_values))

                d[fieldset_name][field_name] = field_values

        return d


    def spider_idle(self, spider):
        if not spider.idle:
            spider.idle = True
            for url in spider.start_urls:
                r = scrapy.Request(url, callback=spider.parse_html)
                spider.crawler.engine.crawl(r, spider)

            raise DontCloseSpider()


def clean_text(text):
    remove_regex = '(®|™)'

    text = re.sub(remove_regex, '', text.strip())

    return text


def get_url(urls, cl):
    terms = [t for t in cl.split(' ') if t.startswith('term')]
    entities = [t for t in cl.split(' ') if t.startswith('entity')]

    url = None

    if terms:
        term = terms[0]
        term_id = re.sub('term-', '', term)

        if term_id in urls:
            url = urls[term_id]

    if not url and entities:
        entity = entities[0]
        entity_id = re.sub('entity-', '', entity)

        url = 'https://www.amd.com/en/product/{}'.format(entity_id)

    return url
